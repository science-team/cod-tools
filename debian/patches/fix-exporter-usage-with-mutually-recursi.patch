From: Niko Tyni <ntyni@debian.org>
Date: Tue, 6 Aug 2024 20:38:09 +0100
X-Dgit-Generated: 3.10.0+dfsg-1 5a1458da5fc72e0d208dc9baa32a05fe79a92403
Subject: Fix Exporter usage with mutually recursive modules

This fixes warnings with Perl 5.40:

  Attempt to call undefined import method with arguments ("cif_cell_contents") via package "COD::CIF::Data::CellContents" (Perhaps you forgot to load the package?)

See the "Playing Safe" section in Exporter(3perl) and

  https://metacpan.org/dist/perl/view/pod/perldelta.pod#Calling-the-import-method-of-an-unknown-package-produces-a-warning


Bug-Debian: https://bugs.debian.org/1078097

---

diff --git a/src/lib/perl5/COD/CIF/Data/CellContents.pm b/src/lib/perl5/COD/CIF/Data/CellContents.pm
index 27d7ced..090d6b7 100644
--- a/src/lib/perl5/COD/CIF/Data/CellContents.pm
+++ b/src/lib/perl5/COD/CIF/Data/CellContents.pm
@@ -14,6 +14,17 @@ package COD::CIF::Data::CellContents;
 
 use strict;
 use warnings;
+
+BEGIN {
+require Exporter;
+our @ISA = qw( Exporter );
+our @EXPORT_OK = qw(
+    cif_cell_contents
+    atomic_composition
+    print_composition
+);
+}
+
 use COD::AtomProperties;
 use COD::Fractional qw( symop_ortho_from_fract ) ;
 use COD::Spacegroups::Symop::Parse qw( symop_from_string
@@ -26,14 +37,6 @@ use COD::CIF::Data::AtomList qw( atom_array_from_cif );
 use COD::CIF::Data::EstimateZ qw( cif_estimate_z );
 use COD::CIF::Data::SymmetryGenerator qw( symop_generate_atoms );
 
-require Exporter;
-our @ISA = qw( Exporter );
-our @EXPORT_OK = qw(
-    cif_cell_contents
-    atomic_composition
-    print_composition
-);
-
 $::format = "%g";
 
 sub atomic_composition( $$$@ );
diff --git a/src/lib/perl5/COD/CIF/Data/EstimateZ.pm b/src/lib/perl5/COD/CIF/Data/EstimateZ.pm
index 1f82159..42f1349 100644
--- a/src/lib/perl5/COD/CIF/Data/EstimateZ.pm
+++ b/src/lib/perl5/COD/CIF/Data/EstimateZ.pm
@@ -16,6 +16,15 @@ package COD::CIF::Data::EstimateZ;
 
 use strict;
 use warnings;
+BEGIN {
+require Exporter;
+our @ISA = qw( Exporter );
+our @EXPORT_OK = qw(
+    cif_estimate_z
+    cif_estimate_z_from_formula
+);
+}
+
 use COD::Cell qw( cell_volume );
 use COD::CIF::Data qw( get_cell get_symmetry_operators );
 use COD::Precision qw( unpack_cif_number );
@@ -29,13 +38,6 @@ use COD::Spacegroups::Symop::Parse qw( symop_from_string
 use COD::CIF::Data::SymmetryGenerator qw( symop_generate_atoms );
 use COD::CIF::Data::CellContents qw( cif_cell_contents );
 
-require Exporter;
-our @ISA = qw( Exporter );
-our @EXPORT_OK = qw(
-    cif_estimate_z
-    cif_estimate_z_from_formula
-);
-
 # Avogadro number in "CIF unit" scale:
 my $N = 0.1 * 6.0221418;
 
