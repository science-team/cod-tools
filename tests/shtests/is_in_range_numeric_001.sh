#!/bin/sh

#BEGIN DEPEND------------------------------------------------------------------
INPUT_MODULE=src/lib/perl5/COD/CIF/DDL/Ranges.pm
#END DEPEND--------------------------------------------------------------------

IMPORT_MODULE=$(\
    echo ${INPUT_MODULE} | \
    perl -pe "s|^src/lib/perl5/||; s/[.]pm$//; s|/|::|g;" \
)

perl -M"${IMPORT_MODULE}" \
<<'END_SCRIPT'
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2024-05-26 17:12:38 +0300 (Sun, 26 May 2024) $ 
#$Revision: 10066 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/is_in_range_numeric_001.sh $
#------------------------------------------------------------------------------
#*
#* Unit test for the COD::CIF::DDL::Ranges::is_in_range_numeric subroutine.
#* Tests the way the subroutine behaves when the input value is a special
#* type of number (inf, NaN);
#**

use strict;
use warnings;

# use COD::CIF::DDL::Ranges;

my $range = [ -50, undef ];

my @sanity_values = (
    42,
    -420.000,
    420.000,
);

my @test_values = (
    'test',
    'inf',
    '+inf',
    '-inf',
    'infinity',
    '+infiNity',
    '-infinity',
    'NaN',
    '+nan',
    '-naN',
);

print "Sanity values:\n";
for my $value (@sanity_values) {
    print $value, ': ';
    if ( COD::CIF::DDL::Ranges::is_in_range_numeric( $value, { 'range' => $range } ) ) {
        print 'in range';
    } else {
        print 'not in range';
    }
    print "\n";
}

print "Test values:\n";
for my $value (@test_values) {
    print $value, ': ';
    if ( COD::CIF::DDL::Ranges::is_in_range_numeric( $value, { 'range' => $range } ) ) {
        print 'in range';
    } else {
        print 'not in range';
    }
    print "\n";
}

END_SCRIPT
