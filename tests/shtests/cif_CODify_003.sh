#!/bin/sh

#BEGIN DEPEND------------------------------------------------------------------
INPUT_SCRIPT=scripts/cif_CODify
INPUT_CIF=""
#END DEPEND--------------------------------------------------------------------

#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2024-05-26 17:12:38 +0300 (Sun, 26 May 2024) $ 
#$Revision: 10066 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/cif_CODify_003.sh $
#------------------------------------------------------------------------------
#**
#* Tests the way the 'cif_CODify' script behaves when the input CIF file
#* is not provided.
#**

set -ue

PATH=.:${PATH}

${INPUT_SCRIPT} "${INPUT_CIF}"
