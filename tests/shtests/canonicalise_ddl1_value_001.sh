#!/bin/sh

#BEGIN DEPEND------------------------------------------------------------------
INPUT_MODULE=src/lib/perl5/COD/CIF/DDL/DDL1.pm
#END DEPEND--------------------------------------------------------------------

IMPORT_MODULE=$(\
    echo ${INPUT_MODULE} | \
    perl -pe "s|^src/lib/perl5/||; s/[.]pm$//; s|/|::|g;" \
)

perl -M"${IMPORT_MODULE} qw( canonicalise_value )" \
<<'END_SCRIPT'
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2024-05-26 17:12:38 +0300 (Sun, 26 May 2024) $ 
#$Revision: 10066 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/canonicalise_ddl1_value_001.sh $
#------------------------------------------------------------------------------
#*
#* Unit test for the COD::CIF::DDL::DDL1::canonicalise_value subroutine.
#* Tests the way the subroutine behaves when the input value is a special
#* type of number (inf, NaN);
#**

use strict;
use warnings;

# use COD::CIF::DDL::DDL1 qw( canonicalise_value );

my @sanity_values = (
    42,
    42.000,
    42.000,
);

my @test_values = (
    'test',
    'inf',
    '+inf',
    '-inf',
    'infinity',
    '+infiNity',
    '-infinity',
    'NaN',
    '+nan',
    '-naN'
);

print "Sanity values:\n";
for my $value (@sanity_values) {
    print canonicalise_value( $value, 'numb'), "\n";
}

print "Test values:\n";
for my $value (@test_values) {
    print canonicalise_value( $value, 'numb'), "\n";
}

END_SCRIPT
