#!/bin/sh

#BEGIN DEPEND------------------------------------------------------------------
INPUT_MANAGE_MODULE=src/lib/perl5/COD/CIF/Tags/Manage.pm
INPUT_MERGE_MODULE=src/lib/perl5/COD/CIF/Tags/Merge.pm
INPUT_PRINT_MODULE=src/lib/perl5/COD/CIF/Tags/Print.pm
#END DEPEND--------------------------------------------------------------------

IMPORT_MANAGE_MODULE=$(\
    echo ${INPUT_MANAGE_MODULE} | \
    perl -pe "s|^src/lib/perl5/||; s/[.]pm$//; s|/|::|g;" \
)

IMPORT_MERGE_MODULE=$(\
    echo ${INPUT_MERGE_MODULE} | \
    perl -pe "s|^src/lib/perl5/||; s/[.]pm$//; s|/|::|g;" \
)

IMPORT_PRINT_MODULE=$(\
    echo ${INPUT_PRINT_MODULE} | \
    perl -pe "s|^src/lib/perl5/||; s/[.]pm$//; s|/|::|g;" \
)

perl -M"${IMPORT_MANAGE_MODULE} qw( new_datablock set_loop_tag set_tag )" \
     -M"${IMPORT_MERGE_MODULE}  qw( merge_datablocks )" \
     -M"${IMPORT_PRINT_MODULE}  qw( print_cif )" \
<<'END_SCRIPT'
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2024-05-26 17:12:38 +0300 (Sun, 26 May 2024) $ 
#$Revision: 10066 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/merge_datablocks_001.sh $
#------------------------------------------------------------------------------
#*
#* Unit test for the COD::CIF::Tags::Merge::merge_datablocks() subroutine.
#* Tests overwriting a looped data item with a non-looped one.
#**

use strict;
use warnings;

# use COD::CIF::Tags::Manage qw( new_datablock set_loop_tag set_tag );
# use COD::CIF::Tags::Merge qw( merge_datablocks );
# use COD::CIF::Tags::Print qw( print_cif );

my $data_block_single = new_datablock( 'single' );
my $data_block_loop = new_datablock( 'loop' );

set_tag( $data_block_single, '_data_name', 'value' );
set_loop_tag( $data_block_loop, '_data_name', undef, [ 1, 2, 3 ] );

merge_datablocks( $data_block_single, $data_block_loop, { override_all => 1 } );
print_cif( $data_block_loop );

END_SCRIPT
