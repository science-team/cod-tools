#!/bin/sh

#BEGIN DEPEND------------------------------------------------------------------
INPUT_MODULE=src/lib/perl5/COD/CIF/Tags/Print.pm
#END DEPEND--------------------------------------------------------------------

IMPORT_MODULE=$(\
    echo ${INPUT_MODULE} | \
    perl -pe "s|^src/lib/perl5/||; s/[.]pm$//; s|/|::|g;" \
)

perl -M"${IMPORT_MODULE}" \
<<'END_SCRIPT'
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2024-05-26 17:12:38 +0300 (Sun, 26 May 2024) $
#$Revision: 10066 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/sprint_loop_packet_001.sh $
#------------------------------------------------------------------------------
#*
#* Unit test for the COD::CIF::Tags::Print::sprint_loop_packet() subroutine.
#* Tests the way the subroutine behaves when the packet consists of a single
#* data item of various types.
#**

use strict;
use warnings;

use COD::CIF::Tags::Print;

my $options = {
    'fold_long_fields' => 0,
    'folding_width'    => 80,
    'cif_version'      => '1.1',
};
my $test_tag = '_tag';

my $data_block = {
  'name' => 'test',
  'cifversion' => {
    'major' => 1,
    'minor' => 1
  },
  'inloop' => {
    "$test_tag" => 0
  },
  'loops' => [
    [
      "$test_tag"
    ]
  ],
  'precisions' => {},
  'save_blocks' => [],
  'tags' => [
    "$test_tag"
  ],
  'types' => {
    "$test_tag" => [
      'UQSTRING'
    ]
  },
  'values' => {
    "$test_tag" => [
      'Placeholder.'
    ]
  }
};

print "# Multiline text field value\n";
print "loop_\n";
print "_tag\n";
$data_block->{'values'}{$test_tag}[0] = "\nMultiline\nvalue";
$data_block->{'types'}{$test_tag}[0] = 'TEXTFIELD';
print COD::CIF::Tags::Print::sprint_loop_packet(
                    $data_block,
                    [ $test_tag ],
                    0,
                    $options,
                );

print "# Long value\n";
print "loop_\n";
print "_tag\n";
$data_block->{'values'}{$test_tag}[0] = 'Loop-value-that-is-longer-than-the-maximum-line-length-and-is-thus-handled-differently.';
$data_block->{'types'}{$test_tag}[0] = 'UQSTRING';
print COD::CIF::Tags::Print::sprint_loop_packet(
                    $data_block,
                    [ $test_tag ],
                    0,
                    $options,
                );

print "# Short value\n";
print "loop_\n";
print "_tag\n";
$data_block->{'values'}{$test_tag}[0] = 'Short value';
$data_block->{'types'}{$test_tag}[0] = 'SQSTRING';
print COD::CIF::Tags::Print::sprint_loop_packet(
                    $data_block,
                    [ $test_tag ],
                    0,
                    $options,
                );

END_SCRIPT
