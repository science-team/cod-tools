#!/bin/sh
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2024-05-26 17:12:38 +0300 (Sun, 26 May 2024) $ 
#$Revision: 10066 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/cod_manage_related_001.sh $
#------------------------------------------------------------------------------
#*
#* Tests the way the 'cod_manage_related' script handles reading from STDIN.
#**

#BEGIN DEPEND------------------------------------------------------------------
INPUT_SCRIPT=scripts/cod_manage_related
INPUT_CIF=tests/inputs/related_empty_entry.cif
#END DEPEND--------------------------------------------------------------------

set -ue

PATH=.:${PATH}

cat ${INPUT_CIF} | ${INPUT_SCRIPT} \
--related-entry-database    "COD on a ROD" \
--related-entry-code        "F15H" \
--related-entry-description "It might struggle!" \
--related-entry-uri         "http://www.fishonastick.com"
