#!/bin/sh
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2020-01-01 08:42:18 +0200 (Wed, 01 Jan 2020) $ 
#$Revision: 7637 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/tests/shtests/cif_find_duplicates_011.sh $
#------------------------------------------------------------------------------
#*
#* Test the way the 'cif_find_duplicates' handles entries that have been
#* explicitly marked as enantiomers by the COD maintainers both in the
#* checked entries and in the reference entries.
#**

#BEGIN DEPEND------------------------------------------------------------------
INPUT_SCRIPT=scripts/cif_find_duplicates
INPUT_NEW_CIF_DIR=tests/inputs/cif_find_duplicates/enantiomers/marked
INPUT_REF_CIF_DIR=tests/inputs/cif_find_duplicates/enantiomers/marked
#END DEPEND--------------------------------------------------------------------

set -ue

unset LANG
unset LC_CTYPE

${INPUT_SCRIPT} \
    ${INPUT_NEW_CIF_DIR} \
    ${INPUT_REF_CIF_DIR} \
| sort
