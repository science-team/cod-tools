/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/stringx.h $
\*---------------------------------------------------------------------------*/

#ifndef __STRINGX_H
#define __STRINGX_H

#include <cexceptions.h>

char *strdupx( const char *str, cexception_t *ex );

#endif
