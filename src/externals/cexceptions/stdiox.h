/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/stdiox.h $
\*---------------------------------------------------------------------------*/

#ifndef _STDIOX_H
#define _STDIOX_H

#include <stdio.h>
#include <cexceptions.h>

extern void *stdiox_subsystem;

typedef enum {
  STDIOX_OK = 0,
  STDIOX_FILE_OPEN_ERROR,
  STDIOX_FILE_CLOSE_ERROR,
  STDIOX_FILE_MEMOPEN_ERROR,

  STDIOX_ERROR_last
} STDIOX_ERROR;

FILE *fopenx( const char *filename, const char *mode, cexception_t *ex );
void fclosex( FILE *file, cexception_t *ex );
FILE *fmemopenx( void *buf, size_t size, const char *mode, cexception_t *ex );

#endif
