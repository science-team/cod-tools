/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-07-30 20:10:23 +0300 (Fri, 30 Jul 2021) $
** $Revision: 8841 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/stdiox.c $
\*---------------------------------------------------------------------------*/

/* exports: */
#include <stdiox.h>

/* uses: */
#include <cexceptions.h>
#include <cxprintf.h>
#include <errno.h>
#include <string.h>

void *stdiox_subsystem = &stdiox_subsystem;

FILE *fopenx( const char *filename, const char *mode, cexception_t *ex )
{
    FILE *f = fopen( filename, mode );

    if( f == NULL ) {
        cexception_raise_syserror
            ( ex, stdiox_subsystem, STDIOX_FILE_OPEN_ERROR,
              "could not open file", strerror( errno ));
    }

    return f;
}

void fclosex( FILE *file, cexception_t *ex )
{
    if( fclose( file ) != 0 ) {
        cexception_raise_syserror
            ( ex, stdiox_subsystem, STDIOX_FILE_CLOSE_ERROR,
              "could not close file", strerror( errno ));
    }
}

FILE *fmemopenx( void *buf, size_t size, const char *mode, cexception_t *ex )
{
    FILE *f = fmemopen( buf, size, mode );

    if( f == NULL ) {
        cexception_raise_syserror
            ( ex, stdiox_subsystem, STDIOX_FILE_MEMOPEN_ERROR,
              "could not open file in memory", strerror( errno ));
    }

    return f;
}
