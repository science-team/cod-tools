/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/cxprintf.h $
\*---------------------------------------------------------------------------*/

#ifndef __CEX_REPORT_H
#define __CEX_REPORT_H

#include <stdarg.h>

const char* cxprintf( const char * format, ... );
const char* vcxprintf( const char * format, va_list args );

#endif
