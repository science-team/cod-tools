/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/tests/subsystem_a.h $
\*---------------------------------------------------------------------------*/

#ifndef _SUBSYSTEM_A_H
#define _SUBSYSTEM_A_H

#include <cexceptions.h>

enum SUBSYS_A_ERROR_CODE {
    SUBSYS_A_OK    = 0,
    SUBSYS_A_ERROR = 1
};

extern void *subsystem_a_tag;

void subsystem_a_function( cexception_t *ex );

#endif
