/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/tests/subsystem_a.c $
\*---------------------------------------------------------------------------*/

#include <cexceptions.h>
#include <subsystem_a.h>

void *subsystem_a_tag = &subsystem_a_tag;

#define subsystem_a_exception( EX, MESSAGE ) \
    cexception_raise_in( EX, subsystem_a_tag, SUBSYS_A_ERROR, MESSAGE )

void subsystem_a_function( cexception_t *ex )
{
    subsystem_a_exception( ex, "Error in subsystem A function" );
}
