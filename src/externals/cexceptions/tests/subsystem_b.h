/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/tests/subsystem_b.h $
\*---------------------------------------------------------------------------*/

#ifndef _SUBSYSTEM_B_H
#define _SUBSYSTEM_B_H

#include <cexceptions.h>

enum SUBSYS_B_ERROR_CODE {
    SUBSYS_B_OK    = 0,
    SUBSYS_B_ERROR = 1
};

extern void *subsystem_b_tag;

void subsystem_b_function( cexception_t *ex );

#endif
