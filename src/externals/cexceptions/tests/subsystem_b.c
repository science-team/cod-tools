/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/tests/subsystem_b.c $
\*---------------------------------------------------------------------------*/

#include <cexceptions.h>
#include <subsystem_b.h>

void *subsystem_b_tag = &subsystem_b_tag;

#define subsystem_b_exception( EX, MESSAGE ) \
    cexception_raise_in( EX, subsystem_b_tag, SUBSYS_B_ERROR, MESSAGE )

void subsystem_b_function( cexception_t *ex )
{
    subsystem_b_exception( ex, "Error in subsystem B function" );
}
