/*---------------------------------------------------------------------------*\
** $Author: antanas $
** $Date: 2021-08-25 14:49:05 +0300 (Wed, 25 Aug 2021) $
** $Revision: 8871 $
** $URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/externals/cexceptions/tests/programs/tcreallocx.c $
\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stringx.h>
#include <allocx.h>
#include <cexceptions.h>

int main()
{
    int *data = NULL;
    int i;
    const int len = 20;

    for( i = 0; i < len; i++ ) {
        data = creallocx( data, i, i + 1, sizeof(data[0]), NULL );
        data[i] += i;
    }

    for( i = 0; i < len; i++ ) {
        printf( "%d ", data[i] );
    }
    printf( "\n" );

    freex( data );

    return 0;
}
