#!/usr/bin/python3
# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------
#$Author: antanas $
#$Revision: 8230 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/components/pycodcif/tests/shtests/tag_index_002.sh $
#$Date: 2020-07-20 16:43:00 +0300 (Mon, 20 Jul 2020) $
#$Id: tag_index_002.sh 8230 2020-07-20 13:43:00Z antanas $
#------------------------------------------------------------------------------
#*
#  Test driver for pycodcif module.
#**
import sys
from pycodcif import CifFile, CifDatablock

datablock = CifDatablock("new")
datablock['_tag'] = 10

print( datablock['_tag'] )

cif = CifFile()
cif.append( datablock )
print( cif )

print( cif['new']['_tag'] )
print( cif[0]['_tag'] )
print( cif[0].keys() )
