#!/usr/bin/python3
# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------
#$Author: antanas $
#$Revision: 8230 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/components/pycodcif/tests/shtests/build_cif_001.sh $
#$Date: 2020-07-20 16:43:00 +0300 (Mon, 20 Jul 2020) $
#$Id: build_cif_001.sh 8230 2020-07-20 13:43:00Z antanas $
#------------------------------------------------------------------------------
#*
#  Test driver for pycodcif module.
#**
import sys
from pycodcif import new_cif, cif_start_datablock, datablock_insert_cifvalue, cif_datablock_list, cif_print

cif = new_cif( None )
cif_start_datablock( cif, "first", None )

datablock_insert_cifvalue( cif_datablock_list( cif ), "_number",  12.3456789, None )
datablock_insert_cifvalue( cif_datablock_list( cif ), "_string",  "Someone Else", None )
datablock_insert_cifvalue( cif_datablock_list( cif ), "_unknown", None, None )

cif_print( cif )
