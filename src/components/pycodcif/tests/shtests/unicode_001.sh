#!/usr/bin/python3
# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------
#$Author: antanas $
#$Revision: 8230 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/components/pycodcif/tests/shtests/unicode_001.sh $
#$Date: 2020-07-20 16:43:00 +0300 (Mon, 20 Jul 2020) $
#$Id: unicode_001.sh 8230 2020-07-20 13:43:00Z antanas $
#------------------------------------------------------------------------------
#*
#  Test driver for pycodcif module.
#**
import sys
from pycodcif import parse, CifParserException

filename = sys.argv.pop()

try:
    parse( "sąžininga žąsis" )
except CifParserException as e:
    pass

try:
    parse( u"sąžininga žąsis" )
except CifParserException as e:
    pass
