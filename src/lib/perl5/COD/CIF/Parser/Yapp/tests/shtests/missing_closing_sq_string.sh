#! /bin/sh
#!perl -w # --*- Perl -*--
eval 'exec perl -x $0 ${1+"$@"}'
    if 0;
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2019-11-15 19:56:35 +0200 (Fri, 15 Nov 2019) $ 
#$Revision: 7422 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/lib/perl5/COD/CIF/Parser/Yapp/tests/shtests/missing_closing_sq_string.sh $
#------------------------------------------------------------------------------
#*
#  Perl test driver for testsing how strings with a missing single closing
#  quote are fixed.
#**

use strict;
use warnings;

use File::Basename;
use COD::CIF::Parser::Yapp;

my $script_dir  = File::Basename::dirname( $0 );
my $script_name = File::Basename::basename( $0 );

$script_name =~ s/\.sh$//;

my $filename = "${script_dir}/${script_name}.inp";

my $parser = new COD::CIF::Parser::Yapp;

my $data = $parser->Run($filename, {
                            'fix_missing_closing_single_quote' => 1,
                        });

for my $tag (@{$data->[0]{tags}}) {
    for my $value (@{$data->[0]{values}{$tag}}) {
        print ">> $tag '$value' \n";
    }
}
