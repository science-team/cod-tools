#! /bin/sh
#!perl -w # --*- Perl -*--
eval 'exec perl -x $0 ${1+"$@"}'
    if 0;
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2019-11-15 19:56:35 +0200 (Fri, 15 Nov 2019) $ 
#$Revision: 7422 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/lib/perl5/COD/CIF/Parser/Yapp/tests/shtests/unquoted_string_with_brackets.sh $
#------------------------------------------------------------------------------
#*
#  Perl test driver.
#**

use strict;
use warnings;

use File::Basename;
use COD::CIF::Parser::Yapp;

my $script_dir  = File::Basename::dirname( $0 );
my $script_name = File::Basename::basename( $0 );

$script_name =~ s/\.sh$//;

my $filename = "${script_dir}/${script_name}.inp";

my $parser = new COD::CIF::Parser::Yapp;

my %options;

$options{allow_uqstring_brackets} = 1;

my $data = $parser->Run($filename, \%options);

for my $k (sort keys %{$data->[0]{values}}) {
    my $v = $data->[0]{values}{$k};
    print "Values for '$k':\n";
    for my $mas(@{$v}) {
        print $mas . "\n";
    }
    print "-" x 20 . "\n";
}

exit 0;
