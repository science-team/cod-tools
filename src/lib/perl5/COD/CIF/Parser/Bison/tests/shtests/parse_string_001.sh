#!/bin/sh
#!perl -w # --*- Perl -*--
eval 'exec perl -x $0 ${1+"$@"}'
    if 0;
#------------------------------------------------------------------------------
#$Author: antanas $
#$Date: 2021-07-30 19:52:52 +0300 (Fri, 30 Jul 2021) $ 
#$Revision: 8840 $
#$URL: svn+ssh://www.crystallography.net/home/coder/svn-repositories/cod-tools/tags/v3.10.0/src/lib/perl5/COD/CIF/Parser/Bison/tests/shtests/parse_string_001.sh $
#------------------------------------------------------------------------------
#*
#  Perl test driver used to check the way precision values are stored.
#**

use strict;
use warnings;

use File::Basename;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
use COD::CIF::Parser::Bison;

print Dumper COD::CIF::Parser::Bison::parse_string( "data_test _tag value" );
