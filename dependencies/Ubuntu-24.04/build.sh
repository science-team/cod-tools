#!/bin/sh

sudo apt-get install -y \
    autoconf \
    bison \
    cmake \
    gcc \
    libmodule-scandeps-perl \
    libnauty-dev \
    libparse-yapp-perl \
    libtool \
    make \
    perl \
    python3-dev \
    python3-setuptools \
    swig \
;
